package vito.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vito.constants.Constant;
import vito.models.Event;
import vito.services.EventService;

/**
 * @author Vitaliy Ippolitov
 */
@RestController
public class TaskController {

    @Autowired
    private EventService eventService;

    @RequestMapping(value = "/taskHistory/create", method = RequestMethod.POST, consumes =
        "application/json")
    public ResponseEntity create(@RequestBody Event event, BindingResult bindingResult) {
        if (bindingResult.hasErrors() || event == null) {
            return ResponseEntity.badRequest().body(Constant.BINDING_ERR_RU);
        }

        final String body = eventService.create(event);
        if (body.isEmpty()) {
            return ResponseEntity.ok("");
        }
        return ResponseEntity.badRequest().body(Constant.SERVER_ERR_RU + body);
    }
}
