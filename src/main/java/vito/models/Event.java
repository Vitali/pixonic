package vito.models;

import java.time.LocalDateTime;
import java.util.concurrent.Callable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Vitaliy Ippolitov
 */
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Data
public class Event {
    private LocalDateTime localDateTime;
    private Callable callable;
}
