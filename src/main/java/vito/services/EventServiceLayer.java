package vito.services;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.stereotype.Service;

import vito.constants.Constant;
import vito.models.Event;

/**
 * @author Vitaliy Ippolitov
 */
@Service
public class EventServiceLayer implements EventService {

    private Map<LocalDateTime, List<Callable>> futureEventMap;
    private Scheduler scheduler;

    public static final short EVENT_PLANNING_LIMIT = 1000;

    public EventServiceLayer() throws SchedulerException {
        futureEventMap = new ConcurrentHashMap<>();
        scheduler = new StdSchedulerFactory().getScheduler();
        scheduler.start();
    }

    @Override
    public long getFutureEventMapSize() {
        return futureEventMap.values()
            .stream().mapToLong(List::size).sum();
    }

    @Override
    public String create(Event event) {
        if (event == null || event.getCallable() == null || event.getLocalDateTime() == null) {
            return Constant.BINDING_ERR_RU;
        }

        final LocalDateTime key = event.getLocalDateTime();
        final List<Callable> value = Optional.ofNullable(
            futureEventMap.get(key)
        ).orElse(new ArrayList<>(1));

        try {
            Trigger trigger;
            if (value.size() < EVENT_PLANNING_LIMIT) {
                value.add(event.getCallable());
                futureEventMap.put(key, value);

                trigger = TriggerBuilder.newTrigger()
                    .startAt(new Date(
                        key.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli())
                    )
                    .startNow()
                    .build();
            } else {
                trigger = TriggerBuilder.newTrigger()
                    .startNow()
                    .build();
            }
            addExecutor(trigger);
            return "";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    private void addExecutor(Trigger trigger) throws SchedulerException {
        final JobDetail job = JobBuilder.newJob(EventServiceLayer.Executor.class)
            .build();

        scheduler.scheduleJob(job, trigger);
    }

    private class Executor implements Job {
        @Override
        public void execute(JobExecutionContext ctx) throws JobExecutionException {
            try {
                final LocalDateTime key = LocalDateTime.ofInstant(
                    Instant.ofEpochMilli(ctx.getJobRunTime()), ZoneId.systemDefault()
                );
                final List<Callable> value = Optional.ofNullable(futureEventMap.get(key))
                    .orElseThrow(IndexOutOfBoundsException::new);

                value.forEach(c -> {
                    try {
                        c.call();
                        value.remove(c);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                futureEventMap.put(key, value); // update map (it needed when c.call throw ex)
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
