package vito.services;

import vito.models.Event;

/**
 * @author Vitaliy Ippolitov
 */
public interface EventService {

    /**
     * Create event for run in scheduler
     * @return empty String if success
     */
    String create(Event event);

    long getFutureEventMapSize();
}
