package vito.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;

import vito.constants.Constant;
import vito.models.Event;
import vito.services.EventService;
import vito.services.EventServiceLayer;

/**
 * @author Vitaliy Ippolitov
 */

@RunWith(SpringRunner.class)
@WebMvcTest
public class EventServiceTest {

    @Autowired
    private EventService eventService;

    @Test
    public void appContextTest() {
        // application context and init servlets test (may with empty body)
    }

    @Test
    public void testServiceAutowiredCorrect() {
        assertNotNull(eventService);
    }

    @Test
    public void testCreateFutureEmptyEvent() {
        final long sizeBefore = eventService.getFutureEventMapSize();
        final String stringResult = eventService.create(null);
        assertEquals(eventService.getFutureEventMapSize(), sizeBefore);
        assertEquals(stringResult, Constant.BINDING_ERR_RU);
    }

    private void testAddFutureOneEvent(int expectedIncrement) {
        final long sizeBefore = eventService.getFutureEventMapSize();
        final Event event = new Event(LocalDateTime.of(2222, 1, 1, 1, 1), () -> null);
        final String stringResult = eventService.create(event);
        assertEquals(eventService.getFutureEventMapSize(), sizeBefore + expectedIncrement);
        assertEquals(stringResult, "");
    }

    @Test
    public void testCreateFutureAfterLimitCountEvent() {
        int count = EventServiceLayer.EVENT_PLANNING_LIMIT - 1;
        do {
            testAddFutureOneEvent(1);
        } while (count-- > 0);

        count = 20;
        do {
            testAddFutureOneEvent(0);
        } while (--count > 0);
    }
}
